# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `user_guess a number`. Each time through a play loop,
#   get a user_guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
	puts "guess a number"
	user_guess = gets.chomp.to_i
	number = rand(1..100)
	prev_guess = 0
	num_guesses = 1
	until user_guess == number
		if user_guess < number
			puts "#{user_guess} too low"
		elsif user_guess > number
			puts "##{user_guess} too high"
		elsif user_guess == prev_guess
			raise "guess can't be the same as previous guess"
		elsif user_guess == 0
			raise "can't be zero"
		end
		prev_guess = user_guess
		num_guesses += 1
		puts "guess a number"
		user_guess = gets.chomp.to_i
	end
	puts "#{user_guess} guessed in #{guesses} guesses"
end
